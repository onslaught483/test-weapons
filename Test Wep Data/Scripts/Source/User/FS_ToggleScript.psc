Scriptname FS_ToggleScript extends activemagiceffect

ActorValue Property SwitchVal Auto Const
Potion Property Pot Auto Const

Event OnEffectStart(Actor akTarget, Actor akCaster)
      bool lastVal = akTarget.getvalue(SwitchVal) as bool
	
	if lastVal
		akTarget.setvalue(SwitchVal, 0)
		debug.notification("OFF")
	else
		akTarget.setvalue(SwitchVal, 1)
		debug.notification("ON")
	endif

	aktarget.additem(Pot,1,true)
endevent